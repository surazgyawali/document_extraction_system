import re
def get_data(pages):
    data = {}
    for idx,line in enumerate(pages[0].splitlines()):
        rechnung_ptrn = re.compile(r'Rechnung\s+\d{1,20}')
        dokdatum_ptrn = re.compile(r'Dokumentdatum\s+\d{2}.\d{2}.\d{4}')
        kundennummer_ptrn = re.compile(r'Kundennummer\s+\d{1,20}')
        zahlbar_ptrn = re.compile(r'Zahlbar bis\s+\d{2}.\d{2}.\d{4}')

        rechObj = re.findall(rechnung_ptrn,line)
        dokObj = re.findall(dokdatum_ptrn,line)
        kundeObj = re.findall(kundennummer_ptrn,line)
        zahObj = re.findall(zahlbar_ptrn,line)
        if 'green.ch' in line:
            data['company_name'] = 'green.ch'
        if rechObj:
            data['rechnung'] = rechObj[0].replace('Rechnung ','').strip()
        if dokObj:
            data['dokumentdatum'] = dokObj[0].replace('Dokumentdatum','').strip()
        if kundeObj:
            data['kundennummer'] = kundeObj[0].replace('Kundennummer','').strip()
        if zahObj:
            data['zahlbar_bis'] = zahObj[0].replace('Zahlbar bis','').strip()

        mwst_ptrn = re.compile(r'MWST-Nummer\s+')
        mwstObj = re.findall(mwst_ptrn,line)
        if mwstObj:
            data['mwst_nummer'] = line.replace('MWST-Nummer','').strip()

        ptrn = re.compile(r'Periodizitat\s+')
        Obj = re.findall(ptrn,line)
        if Obj:
            data['periodizitat'] = line.replace('Periodizitat','').strip()

        if 'Betrag' in line:
            data['betrag'] = line.replace('Betrag','').strip()

        ptrn = re.compile(r'MWST\s+\d{1,}.\d{1,}\s{0,}%')
        Obj = re.findall(ptrn,line)
        if Obj:
            mach = Obj[0].replace(' ','_')
            data[mach] = line[-10:].strip()
        if 'TOTAL CHF' in line:
            data['total_chf'] = line.replace('TOTAL CHF','').strip()
    return data