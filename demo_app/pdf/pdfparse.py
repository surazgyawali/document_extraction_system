# %%
import argparse
import pathlib

import cv2
import pytesseract
import numpy

from pdf2image import convert_from_path
import pdftotext

from pdf import process_image
# %%
CONTENT_PER_PAGE_THRESHOLD = 200

# %%

def get_pages_tesseract(pdf_path):
    pages_txt = []
    pages = convert_from_path(pdf_path, dpi=200, size=(1654, 2340))
    for idx, image in enumerate(pages, 1):
        custom_config = r'-l eng -c preserve_interword_spaces=1 --oem 1 --psm 3'
        print(f"Applying OCR to page {idx}....")
        image = cv2.cvtColor(numpy.array(image), cv2.COLOR_RGB2BGR)
        processed_img = process_image.main(image)
        print(f"Applying OCR to page {idx}....")
        text = pytesseract.image_to_string(
            processed_img,
            config=custom_config
        )
        pages_txt.append(text)
    return pages_txt

# %%
def get_pages_pdftotext(pdf_path):
    with open(pdf_path, "rb") as f:
        pdf = pdftotext.PDF(f)
    return pdf


def get_pages(pdf_path):
    method = 'pdftotext'
    pages = get_pages_pdftotext(pdf_path)
    pages = [page for page in pages]
    total_content_size = len("\n".join(pages).strip())
    avg_content_per_page = total_content_size/len(pages)

    if avg_content_per_page < CONTENT_PER_PAGE_THRESHOLD:
        print(f"Seems to be a scanned PDF, Applying OCR method for this file.\n")
        pages = get_pages_tesseract(pdf_path)
        method = 'OCR'
    return method,pages


def get_pdfTextObj(pdf_path):
    method,pages = get_pages(pdf_path)
    file_meta = {}
    file_meta['method'] = method
    file_meta['pages'] = pages
    return file_meta


def main(input_path):
    input_dir = pathlib.Path(input_path)
    meta_obj = {}
    for file in input_dir.iterdir():
        print(f"\nProcessing File: {file.name}.\n")
        file_obj = get_pdfTextObj(file)
        meta_obj[file.name] = file_obj
    return meta_obj

# %%


if __name__ == '__main__':
    import json
    parser = argparse.ArgumentParser(
        description='Pdf data extraction parameters')

    parser.add_argument('-i',
                        '--input_dir',
                        default='./input',
                        help='Path to pdf data file')

    arguments = parser.parse_args()
    data_path = arguments.input_dir
    out_obj = main(data_path)

    with open('meta.json', 'w') as fp:
        json.dump(out_obj, fp)

    print("Done, Meta infromataion on all pages saved inside meta.json file and new PDFs with bookmarks written in the script's location.")
# %%
