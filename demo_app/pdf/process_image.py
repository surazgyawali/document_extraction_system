import os
import cv2
import argparse
import numpy as np
from PIL import Image

def hist_equalize(img, num_bins = 256):
    hist, bins = np.histogram(img.flatten(), num_bins, normed = False)
    cdf = hist.cumsum()
    cdf = (cdf / cdf[-1]) * 255.

    out_img = np.interp(img.flatten(), bins[:-1], cdf)
    out_img = out_img.reshape(img.shape)
    out_img = out_img.astype(np.uint8)

    return out_img

def color_hist_equalize(img, use_default = True):
    if len(img.shape) == 2:
        return hist_equalize(img)

    yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
    y, u, v = cv2.split(yuv)

    if use_default:
        clahe = cv2.createCLAHE(clipLimit = 2, tileGridSize = (21, 21))
        y = clahe.apply(y)
    else:
        y = hist_equalize(y)

    yuv = cv2.merge((y, u, v))
    out_img = cv2.cvtColor(yuv, cv2.COLOR_YUV2BGR)
    return out_img

def normalize(img):
    max_int, min_int = (np.max(img), np.min(img))

    out_img = (255 * ((img - min_int) / (max_int - min_int)))
    out_img = out_img.astype('uint8')

    return out_img

def shadow_filter(img):
    dilated = cv2.dilate(img, np.ones((7, 7), np.uint8))
    bg = cv2.GaussianBlur(dilated, (15, 15), 5)
    out_img = 255. - cv2.absdiff(img, bg)
    return out_img

def noise_filter(img):
    out_img = normalize(img)
    out_img = cv2.threshold(out_img, 230, 0, cv2.THRESH_TRUNC|cv2.THRESH_BINARY)[1]
    out_img = normalize(out_img)
    return out_img

def display(img):
    if len(img.shape) == 3:
        rgb_img = img[:, :, ::-1]
    else:
        rgb_img = img

    rgb_img = Image.fromarray(rgb_img.astype(np.uint8))
    rgb_img.show()
    input('Enter any key...')

def adjust_gamma(img, gamma = 0.99):
    inv_gamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** inv_gamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    out_img = cv2.LUT(img, table)
    return out_img

def enhance(img, contrast = 16):
    f = 131 * (contrast + 127) / (127 * (131 - contrast))

    alpha_c = f
    gamma_c = 127 * (1 - f)

    out_img = cv2.addWeighted(img, alpha_c, img, 0, gamma_c)

    return out_img

def preprocess(img, output_gray = True):

    img = adjust_gamma(img)
    img = normalize(img)
    img = color_hist_equalize(img, use_default = True)

    out_img = normalize(img)
    out_img = enhance(out_img)
    out_img = shadow_filter(out_img)
    out_img = noise_filter(out_img)

    if output_gray:
        out_img = cv2.cvtColor(out_img, cv2.COLOR_BGR2GRAY)

    return out_img

def main(img):
    out_img = preprocess(img)
    return out_img


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Image Preprocessing Parameters')

    parser.add_argument('-i',
                '--input_path',
                default = './input',
                help = 'Path to data file or directory')

    parser.add_argument('-o',
                '--output_path',
                default = './processed',
                help = 'Path to output directory')

    arguments = parser.parse_args()
    data_path = os.path.abspath(arguments.input_path)
    save_path = os.path.abspath(arguments.output_path)
    print(data_path, save_path)

    main(load_path = data_path, save_path = save_path)
