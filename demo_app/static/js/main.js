var _table_ = document.createElement('table'),
    _tr_ = document.createElement('tr'),
    _th_ = document.createElement('th'),
    _td_ = document.createElement('td');


function buildHtmlTable(arr) {
    var table = _table_.cloneNode(false),
        columns = addAllColumnHeaders(arr, table);
    for (var i = 0, maxi = arr.length; i < maxi; ++i) {
        var tr = _tr_.cloneNode(false);
        for (var j = 0, maxj = columns.length; j < maxj; ++j) {
            var td = _td_.cloneNode(false);
            cellValue = arr[i][columns[j]];
            td.appendChild(document.createTextNode(arr[i][columns[j]] || ''));
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    return table;
}

// Adds a header row to the table and returns the set of columns.
// Need to do union of keys from all records as some records may not contain
// all records
function addAllColumnHeaders(arr, table) {
    var columnSet = [],
        tr = _tr_.cloneNode(false);
    for (var i = 0, l = arr.length; i < l; i++) {
        for (var key in arr[i]) {
            if (arr[i].hasOwnProperty(key) && columnSet.indexOf(key) === -1) {
                columnSet.push(key);
                var th = _th_.cloneNode(false);
                th.appendChild(document.createTextNode(key));
                tr.appendChild(th);
            }
        }
    }
    table.appendChild(tr);
    return columnSet;
}


function get_raw_content(pageList) {
    text_splitted = pageList.join('\n').split('\n')
    text = ''
    text_splitted.forEach(element => {
        // element = element.trim()
        if (element.length > 1) {
            text += '<br>'
            text += element
        }
    });
    return text
}


function refresh_pdf() {
    var rand = Math.floor((Math.random()*1000000)+1);
    var new_url = '/pdf#toolbar=0&navpanes=0&scrollbar=0?uid='+rand;
    $('#pdf').attr('src', new_url);
    $('#pdf').load(new_url);
}


function show_raw_data(pageList, method) {

    text = get_raw_content(pageList)

    titleText = '<span class="text-danger"><b>UH,OH!! The document was not found in our template database so only text with preserved layout was extracted.</b></span>'
    titleText += '<br> <a class="link-success" href="https://docuner.com/"><b>CONTACT US</b></a> to add these type of documents there '
    titleText += 'and do <b>Intelligent Document Processing.</b>'

    if (method == "OCR") {
        titleText += '<br><b> NOTE:</b> Since the PDFs was not digitally created, OCR optimzations might be needed to improve accuracy.'
    }

    if (method == 'pdftotext') {
        titleText += '<br><b> NOTE:</b> Since the PDFs is digitally created, word accuracy is optimal perfectly suits for Intelligent Document Processing.'
    }
    $('#modalLabel')[0].innerHTML = titleText
    $('#output')[0].innerHTML = text
    $('#fmodal').modal('show')

}


function show_structured_data(structured, method) {
    titleText = '<span class="text-success"><b>SUCCESS!!! The document was found in our template database so structured data is extracted.</b></span>'
    titleText += '<br> <a class="link-success" href="https://docuner.com/"><b>CONTACT US</b></a> to use this service and scale your business.'

    if (method == "OCR") {
        titleText += '<br><b> NOTE:</b> Since the PDFs was not digitally created, OCR optimzations might be needed to further improve accuracy.'
    }

    if (method == 'pdftotext') {
        titleText += '<br><b> NOTE:</b> Since the PDFs is digitally created, word accuracy is optimal let us know if some tweaking is needed in the results.'
    }

    titleText += '<br><button id="toggle_structured" type="button" class="btn btn-light btn-sm text-muted">Click here to check raw data.</button>'

    $('#modalLabel')[0].innerHTML = titleText

    $('#output')[0].innerHTML = '<pre><b>' + JSON.stringify(structured, null, 4) + '</b></pre>'
    // $('#output')[0].appendChild(buildHtmlTable([structured]))

    $('#fmodal').modal('show')

    $("#toggle_structured").click(function () {

        if ($(this).hasClass('raw')) {
            $('#output')[0].innerHTML = '<pre><b>' + JSON.stringify(structured, undefined, 2) + '</b></pre>'

            $('#toggle_structured')[0].innerHTML = "Click here to check raw data."
            $(this).removeClass('raw')
            return
        } else {
            var text = get_raw_content(window.pageList)
            $('#output')[0].innerHTML = text
            $('#toggle_structured')[0].innerHTML = "Switch back to structured data."
            $(this).addClass('raw')
            return
        }
    });

}

$(document).ready(function () {

    $("#inputForm").submit(function (event) {
        event.preventDefault();
        var formEl = $(this)
        var form = formEl[0]; // You need to use standard javascript object here
        var formData = new FormData(form);
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $(".progress-bar").width(percentComplete + "%");
                        $("#uploading").text("Uploading: " + percentComplete + "%")
                        if (percentComplete === 100) {
                            $("#uploading").text("")
                        }
                    }
                }, false);
                return xhr;
            },
            beforeSend: function () {

                $("#spinner").show()

            },
            complete: function () {
                $("#spinner").hide();
                $(".progress-bar").hide()
            },
            error: function (xhr, error, thrownError) {
                $("#spinner").hide();
                $(".progress-bar").hide()
                if (xhr.status && xhr.status == 400) {
                    alert(xhr.responseText);
                } else {
                    console.log(xhr, error, thrownError)
                    alert("Your file was rejected, It might be too large for this demo application , try a different file.");
                }
            },
            url: "/",
            type: "POST",
            dataType: 'json', //expected data type
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
        }).done(function (data) {
            var input = document.querySelector('input[type="file"]')
            input.outerHTML=input.outerHTML

            $("#spinner").hide();
            $(".progress-bar").hide()
            var type = data['type']
            if (type == 'info' || type == 'error') {
                var message = data["message"]
                alert(message)
                return
            } else {

                refresh_pdf()

                var method = data['method']
                var pageList = data['pages']
                var structured = data['structured_data']

                window.pageList = pageList
                window.structured = structured

                if (structured !== undefined) {
                    show_structured_data(structured, method)
                    return
                }
                show_raw_data(pageList, method)
                return

            }
        });
    });

});