import os
import uuid
import glob
import secrets
import pathlib

import flask

from werkzeug.utils import secure_filename


UPLOAD_FOLDER = 'uploaded'
OUTPUT_FOLDER = 'output'
ALLOWED_EXTENSIONS = set(['pdf'])

secret_key = secrets.token_urlsafe(16)
app = flask.Flask(__name__)

from pdf import pdfparse
from pdf import template_green_ch

app.secret_key = secret_key
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['OUTPUT_FOLDER'] = OUTPUT_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def upload_form():
    return flask.render_template('index.djhtml')

@app.route('/', methods=['POST'])
def upload_file():
    if flask.request.method == 'POST':
        # check if the post request has the files part
        if 'files[]' not in flask.request.files:
            return flask.jsonify({'type':'info','message':"Uh oh, seems like the file is missing or not proper, Please try again maybe with a different file."})
        files = flask.request.files.getlist('files[]')
        if len(files) > 1:
            message = "Please try a single file,Only One file at a time is allowed for this demo."
            return flask.jsonify({'type':'info','message':message}),200


        upload_folder = app.config['UPLOAD_FOLDER']
        if not os.path.exists(upload_folder):
            os.makedirs(upload_folder)

        curr_files = glob.glob(f'{upload_folder}/*')
        for f in curr_files:
            os.remove(f)

        filename = None
        for file in files[:1]:
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(upload_folder, filename))

        if filename is None:
            return flask.jsonify({
                    "type":"error",
                    "message":"Seems like you uploaded a different file,Please make sure you're uploading PDF file."
                })

        read_path = pathlib.Path(os.path.join(upload_folder,filename))

        try:
            dataObj = pdfparse.get_pdfTextObj(read_path)
            dataObj['type'] = 'data'
            structured_data = None

            try:
                structured_data = template_green_ch.get_data(dataObj['pages'])
                if len(structured_data.keys()) > 2:
                    dataObj['structured_data'] = structured_data
            except Exception as e:
                print(e)
                pass

            return flask.jsonify(dataObj)
        except Exception as e:
            print(str(e))
            return flask.jsonify({"type":"error","message":"Something went wrong from our side, please try again later."})

@app.route('/pdf')
def send_pdf():
    up = pathlib.Path(UPLOAD_FOLDER)
    file = max(up.iterdir(), key = os.path.getctime)
    return flask.send_file(file,cache_timeout=0)

@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0', port=8848)
