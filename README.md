# Instructions

## System requirements

- python **3.6** or later

- pdftotext system dependencies (For instructions: [Here](https://pypi.org/project/pdftotext/))
- Guidelines supporting above link(For windows since it requires a little bit more effort):
  - Install miniconda : [From this link](https://docs.conda.io/en/latest/miniconda.html)
  - Navigate in a browser to [This link](http://visualstudio.microsoft.com/downloads). Under the Tools for Visual Studio 2019 tab download the Build Tools for Visual Studio 2019. You’ll then install the tools by checking the C++ build tools option box and clicking Install. do NOT uncheck any of the automatically selected boxes.

    Run the following two commands.

    ```shell
    conda install -c conda-forge poppler
    ```

    ```shell
    pip install pdftotext
    ```

- tesseract (version 4.1.1)
  - Can be installed using conda since we have minconda already installed.

    ```shell
    conda install -c conda-forge tesseract
    ```

- traineddata eng for tessearct english language (we can use trained data as per our document's language but for now we're using english and that's working fine) in tessdata with proper configuration
    That’s it! You should now have system dependencies installed and ready to go.

## Setup

Install python3 in the system (Do not forget to tick the add to path option.)
Go to project root from  command promt (miniconda prompt will be nicer since we have that) and use below command to install requirements:

(if you have followed conda method do this in conda prompt)

```shell

    pip install -r requirements.txt

```

Once everything is done app can be run.

## Running

Enter following command to run the app when you're inside app root

```shell
    make debug
```

Above will run the webapp in debug mode folder in project root.

 OR

```shell
    make serve
```

Above command will run the app in production mode.

These commands will runt the app in [localhost:8848](http://127.0.0.1:8848/)